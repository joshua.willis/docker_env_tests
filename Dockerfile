FROM registry.gitlab.com/gwburst/public/cwb_docker:compilation_v1.1

USER root
COPY etc/env_setup.sh /etc/zsh/
COPY etc/env_setup.sh /etc/profile.d/
COPY etc/zshrc /tmp/
COPY etc/check_uid.sh /etc/
RUN zsh -ie /etc/check_uid.sh
RUN mkdir -p /.singularity.d/env
COPY etc/zshrc /.singularity.d/env/15-call-env.sh
RUN cat /tmp/zshrc >> /etc/zsh/zshrc && cat /tmp/zshrc >> /etc/bashrc && rm -f /tmp/zshrc

ENV SINGULARITY_ENVIRONMENT '. /etc/profile.d/env_setup.sh'

CMD ["/bin/zsh"]

